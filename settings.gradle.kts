@file:Suppress("UnstableApiUsage")

rootProject.name = "pajato-test"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        mavenLocal()
    }

    // plugins { id("com.pajato.plugins.pcp-kotlin") version "0.9.?" }
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        mavenLocal()
    }
}
