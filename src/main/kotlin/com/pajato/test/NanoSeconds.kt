package com.pajato.test

typealias NanoSeconds = Long

const val ONE_MILLI_SECOND: NanoSeconds = 1000L * 1000L
const val ONE_HUNDRED_MILLI_SECONDS: NanoSeconds = 100L * ONE_MILLI_SECOND
const val FIVE_HUNDRED_MILLI_SECONDS: NanoSeconds = 5L * ONE_HUNDRED_MILLI_SECONDS
