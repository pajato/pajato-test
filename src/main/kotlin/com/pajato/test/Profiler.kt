package com.pajato.test

interface Profiler {
    val result: String
    fun start(name: String, container: String)
    fun stop(threshold: Long, callback: () -> Unit)
}
