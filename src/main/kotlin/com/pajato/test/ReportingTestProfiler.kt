package com.pajato.test

import com.pajato.logger.PajatoLogger
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInfo
import java.io.File

abstract class ReportingTestProfiler(private val threshold: NanoSeconds = ONE_HUNDRED_MILLI_SECONDS) {
    private val profiler: Profiler = TestCaseProfiler()

    @BeforeEach fun setUp(info: TestInfo) { profiler.start(info.displayName, info.testClass.get().simpleName) }

    @AfterEach fun tearDown() { profiler.stop(threshold) { PajatoLogger.log(profiler.result) } }

    fun copyResourceDirs(loader: ClassLoader, srcName: String, dstName: String) {
        val (srcDir, dstDir) = Pair(getDirOrThrow(loader, srcName), getDirOrThrow(loader, dstName))
        srcDir.copyRecursively(dstDir, true)
    }

    private fun getDirOrThrow(loader: ClassLoader, path: String): File {
        val url = loader.getResource(path) ?: throw Exception("Path '$path' is not a valid URI!")
        val file = File(url.toURI())
        if (!file.isDirectory) throw Exception("The file with path '$path' is not a directory!")
        return file
    }
}
