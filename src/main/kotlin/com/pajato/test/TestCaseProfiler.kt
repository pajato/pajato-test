package com.pajato.test

/**
 * The single responsibility for this class is to report the execution time for each test which exceeds a given
 * threshold, defaulted to 100ms.
 */
internal class TestCaseProfiler : Profiler {
    companion object {
        private const val TEN = 10L
        private const val HUNDRED = 100L
        private const val THOUSAND = 1000L
        private const val MILLION = 1000000L
        private const val BILLION = 1000000000L
    }

    override var result: String = ""
    private lateinit var id: String
    private var startTime: Long = 0L
    private var duration: Long = -1L

    override fun start(name: String, container: String) {
        id = if (container.isNotEmpty()) "$container:$name" else name
        result = ""
        startTime = System.nanoTime()
    }

    override fun stop(threshold: Long, callback: () -> Unit) { reportDurationMaybe(threshold, callback) }

    private fun reportDurationMaybe(threshold: Long, callback: () -> Unit) {
        duration = System.nanoTime() - startTime
        if (duration > threshold) doCallback(callback)
    }

    private fun doCallback(callback: () -> Unit) {
        result = "$id duration: ${getFormattedDuration(duration)}"
        callback.invoke()
    }

    internal fun getFormattedDuration(duration: Long): String = when {
        duration < THOUSAND -> "${duration}ns"
        duration < MILLION -> "${duration / THOUSAND}.${getFormattedFraction(duration % THOUSAND)}us"
        duration < BILLION -> "${duration / MILLION}.${getFormattedFraction(duration % MILLION)}ms"
        else -> "${duration / BILLION}.${getFormattedFraction(duration % BILLION)}s"
    }

    private fun getFormattedFraction(fraction: Long): String = when {
        fraction < TEN -> "00$fraction"
        fraction < HUNDRED -> "0$fraction"
        fraction < THOUSAND -> "$fraction"
        fraction < 10 * THOUSAND -> "00$fraction"
        else -> "$fraction"
    }
}
