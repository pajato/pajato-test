package com.pajato.test

import java.io.File
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class CopyResourceDirsUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val dstUrl = loader.getResource("tmp-dir") ?: fail("Could not load src dir resource file!")
        val file = File(dstUrl.toURI())
        removeFilesInDir(file)
        assertEquals(0, file.listFiles()?.size ?: fail("Could not list files!"))
    }

    @Test fun `When copying using an invalid url, verify the exception`() {
        assertFailsWith<Exception> { copyResourceDirs(loader, "no-such_file", "tmp-dir") }
    }

    @Test fun `When copying using a file instead of a directory, verify the exception`() {
        assertFailsWith<Exception> { copyResourceDirs(loader, "empty.txt", "tmp-dir") }
    }

    @Test fun `When copying one directory to another, verify correct behavior`() {
        val srcUrl = loader.getResource("config") ?: fail("Could not load src dir resource file!")
        val dstUrl = loader.getResource("tmp-dir") ?: fail("Could not load src dir resource file!")
        val srcFile = File(srcUrl.toURI())
        val dstFile = File(dstUrl.toURI())
        copyResourceDirs(loader, "config", "tmp-dir")
        compareDirs(srcFile, dstFile)
    }

    private fun removeFilesInDir(dir: File) { dir.walkTopDown().forEach { if (it.isFile) it.delete() } }

    private fun compareDirs(src: File, dst: File) {
        val srcFiles = src.listFiles()?.toList()?.sorted() ?: fail("Source dir has no files!")
        val dstFiles = dst.listFiles()?.toList()?.sorted() ?: fail("Target dir has no files!")
        for (i in srcFiles.indices) {
            val srcFile = srcFiles[i]
            val dstFile = dstFiles[i]
            assertEquals(srcFile.name, dstFile.name)
            assertEquals(srcFile.readText(), dstFile.readText())
        }
    }
}
