package com.pajato.test

import org.junit.jupiter.api.TestInfo
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.assertTrue

abstract class EmptyContainerTestCaseProfiler(private val threshold: NanoSeconds = ONE_HUNDRED_MILLI_SECONDS) {
    private val profiler: Profiler = TestCaseProfiler()

    @BeforeTest fun setUp(info: TestInfo) { profiler.start(info.displayName, "") }

    @AfterTest fun tearDown() { profiler.stop(threshold) { verifyResult() } }

    private fun verifyResult() { assertTrue(profiler.result.isEmpty()) }
}
