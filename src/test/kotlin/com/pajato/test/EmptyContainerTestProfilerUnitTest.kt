package com.pajato.test

import kotlin.test.Test

class EmptyContainerTestProfilerUnitTest : EmptyContainerTestCaseProfiler() {

    @Test fun `When container start `() {
        // an empty test ought to satisfy the code coverage engine.
    }
}
