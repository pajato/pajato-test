package com.pajato.test

import kotlin.test.Test
import kotlin.test.assertEquals

class ProfilerFormattingUnitTest : ReportingTestProfiler(1L) {

    @Test fun `When the test time is reported in nanoseconds, verify result`() {
        val profiler = TestCaseProfiler()
        assertEquals("1ns", profiler.getFormattedDuration(1L))
        assertEquals("12ns", profiler.getFormattedDuration(12L))
        assertEquals("123ns", profiler.getFormattedDuration(123L))
        assertEquals("1.002us", profiler.getFormattedDuration(1002L))
        assertEquals("12.023us", profiler.getFormattedDuration(12023L))
        assertEquals("123.456us", profiler.getFormattedDuration(123456L))
        assertEquals("1.001234ms", profiler.getFormattedDuration(1001234L))
        assertEquals("10.009876ms", profiler.getFormattedDuration(10009876L))
        assertEquals("100.001ms", profiler.getFormattedDuration(100000001L))
        assertEquals("1.234567891s", profiler.getFormattedDuration(1234567891L))
        assertEquals("12.345678912s", profiler.getFormattedDuration(12345678912L))
    }
}
