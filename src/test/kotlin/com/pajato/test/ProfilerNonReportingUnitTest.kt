package com.pajato.test

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlin.test.Test

class ProfilerNonReportingUnitTest : NonReportingTestCaseProfiler(threshold = FIVE_HUNDRED_MILLI_SECONDS) {

    @Test fun `When running a test that will not issue any reports, verify no reports`() {
        runBlocking { delay(1L) }
        runBlocking { delay(10L) }
        runBlocking { delay(100L) }
    }
}
