package com.pajato.test

import kotlin.test.Test

class ProfilerReportingUnitTest : ReportingTestProfiler(1L) {

    @Test fun `When running a test that will issue a report, verify report`() {
        // nop produces the quickest test to guarantee report is generated!
    }
}
