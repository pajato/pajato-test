package com.pajato.test

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlin.test.Test
import kotlin.test.assertEquals

class SerializerTestUnitTest {

    @Test fun `Provide a `() {
        val foo = Foo("data")
        val json = Json.encodeToString(FooSerializer, foo)
        val tester = Tester()
        tester.testEncode(json, FooSerializer, foo)
        tester.testDecode(json, FooSerializer) { assertEquals(foo, it) }
    }

    class Tester : SerializerTest

    @Serializable data class Foo(val data: String)

    object FooSerializer : JsonContentPolymorphicSerializer<Foo>(Foo::class) {
        override fun selectDeserializer(element: JsonElement) = Foo.serializer()
    }
}
