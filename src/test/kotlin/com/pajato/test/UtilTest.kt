package com.pajato.test

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import kotlin.test.Test
import kotlin.test.assertEquals

class UtilTest : ReportingTestProfiler() { // 100ms threshold
    @Test fun convertStringToDate() { assertEquals(1674432000000, "230122190000".convertTimestamp()) }

    @Test fun convertDateToString() { assertEquals("221002110000", 1664766000000L.convertTimestamp()) }

    private fun String.convertTimestamp(): Long {
        val dateFormat = "yyMMddhhmmss"
        return SimpleDateFormat(dateFormat, Locale.getDefault()).parse(this)?.time ?: 0L
    }

    private fun Long.convertTimestamp(): String {
        val date = Date(this)
        val dateFormat = SimpleDateFormat("yyMMddhhmmss")
        return dateFormat.format(date)
    }
}
